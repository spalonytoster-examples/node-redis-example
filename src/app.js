'use strict';
import RedisService from "./redis.service.js";

class MainClass {
  constructor() {
    this.appName = 'Redis Node Example';
    this.redisService = new RedisService();
  }

  async start() {
    this.redisService.connect();
    this.redisService.set('name', this.appName);
    this.redisService.set('version', '1.0.0');
    
    const keys = await this.redisService.getKeys();
    console.log('All keys from Redis:', keys);
    keys.forEach(async key => {
        console.log(`${key}: ${await this.redisService.get(key)}`);
      });

    console.log('All keys from Redis:', await this.redisService.getKeys());

    this.redisService.disconnect();
  }
}

function bootstrap() {
  const mainClass = new MainClass();
  mainClass.start();
}

export default MainClass;
bootstrap();