import { createClient } from 'redis';

class RedisService {
  constructor() {
    this.client = createClient();
  }

  async connect() {
    this.client.on('error', err => console.log('Redis Client Error', err));

    await this.client.connect();
  }

  async disconnect() {
    await this.client.disconnect();
  }

  async set(key, value) {
    await this.client.set(key, value);
  }

  async get(key) {
    return await this.client.get(key);
  }

  async getKeys() {
    return await this.client.keys('*');
  }
}

export default RedisService;