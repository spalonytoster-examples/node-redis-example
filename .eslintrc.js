module.exports = {
    env: {
        "browser": true,
        "commonjs": true,
        "es2021": true
        // "module": true
    },
    extends: ["eslint:recommended", 'plugin:prettier/recommended'],
    overrides: [
    ],
    parserOptions: {
        "ecmaVersion": "latest"
    },
    rules: {
    }
}
